<?php
    // Renvoie le produit de $x et $y
    function produit($x, $y) 
    {
        return $x*$y;
    }
    
    // A FAIRE
    // écrire la fonction permut_valeur
    function permut_valeur($x, $y)
    {
        echo "dans la fonction, avant permutation : x=$x et y=$y <br>";

        $temp = $x;

        $x = $y;

        $y = $temp; 

        echo "dans la fonction, après permutation : x=$x et y=$y <br>";
    }

    function permut_reference(&$x, &$y)
    {
        echo "dans la fonction, avant permutation : x=$x et y=$y <br>";

        $temp = $x;

        $x = $y;

        $y = $temp; 

        echo "dans la fonction, après permutation : x=$x et y=$y <br>";  


    }
    // A FAIRE
    // écrire la fonction permut_reference


?>