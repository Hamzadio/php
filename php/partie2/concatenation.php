<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Initiation au PHP</title>
    <meta charset="utf-8">
</head>
<body>
    <?php
    $nom = "Martin";
    // affichage avec des double quote
    echo "double quote : mon nom est $nom <BR>";
    // affichage avec des simple quote
    echo 'simple quote version 1 : mon nom est $nom <BR>';
    echo 'simple quote version 2 : Mon nom est '.$nom.'<BR>';
    ?>
</body>
</html>