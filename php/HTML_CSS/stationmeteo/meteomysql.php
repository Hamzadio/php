<?php

/**
 * Définition de MyClass
 */
class CMeteoMysql
{

    private $serveur = "localhost";
    private $base = "StationMeteo";
    private $login = "stationmeteo";
    private $motDePasse = "mdpstationmeteo";
    private $idConnexion;
    private $connexionOK = false;
    private $messageErreur = "Error of connection" ;


    public function __construct()
    {
        $this->Connexion();
    }

    private function Connexion()
    {

        //$statement = $pdo->query("SELECT 'Bonjour, cher utilisateur de MySQL !' AS _message FROM DUAL");
        //$row = $statement->fetch(PDO::FETCH_ASSOC);
        //echo htmlentities($row['_message']);


        try 
        {
            $this->idConnexion = new PDO("mysql:host=$this->serveur;dbname=$this->base;charset=utf8mb4", $this->login, $this->motDePasse);
            $this->connexionOK = true;
        } 
        catch (PDOException $e) 
        {
            $this->connexionOK = false;
            $this->messageErreur = "Error of connection : " . $e->getMessage();
        }
    }

    public function getConnexionOK()
    {
        return $this->connexionOK;
    }

    public function getMessageErreur()
    {
        return $this->messageErreur;
    }

    public function Lireall(&$data)
    {
        try 
        {
            $request = "SELECT * FROM Historique ORDER BY horodatage DESC limit 5";
            $statement = $this->idConnexion->query($request);
           // $data = $statement->fetchObject();
            $data = $statement->fetchAll(PDO::FETCH_CLASS);
            return true;
        } 
        catch (PDOException $e) 
        {
            $this->messageErreur = "Error of connection : " . $e->getMessage();
            return false;
        }
    }

    public function __destruct()
    {
        $this->idConnexion = null;
    }

    

}
