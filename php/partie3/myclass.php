<?php

/**
 * Définition de MyClass
 */
class MyClass
{
    public $UnPublic = ' its Public'; //everyone
    protected $UnProtected = ' its Protected';  //the class and the heritage
    private $UnPrivate = 'its Private'; //only the class

    static public $unstatique = 'static';

    
    public function printHello()
    { 
        echo $this->UnPublic;
        echo $this->UnProtected;
        echo $this->UnPrivate;
        
    }
}


?>