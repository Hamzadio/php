<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Initiation au développement Web</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../meteo.css">
	
</head>

<body>
	<header>
		Initiation au développement Web
	</header>
	
	<div class="contenu">
		<nav>
			<a href="../accueil.html" class="clic">Accueil</a>
			<a href="../bases.html">Les bases du langage HTML</a>
			<a href="../tableau.html">Les tableaux</a>
			<a href="../styles_exercices.html">Les styles CSS : exercices</a>
			<a href="../styles_illustration.html">Les styles CSS : illustration</a>
			<a href="../js_exercices.html">Javascript : exercices</a>
			<a href="../js_ampoule.html">Javascript : l'ampoule</a>
			<a href="../js_planets.html">Javascript : planètes</a>
		 	<a href="../stationmeteo/meteo.php">Station météo</a>
		</nav>
			
		<article>

		<h1>Données météorologiques de la station IRDAM WST6000</h1>

		<?php
			
			require_once("meteomysql.php");
			
			 // instanciation d'un objet 
			$MeteoBDD = new CMeteoMysql();
			
			if($MeteoBDD->getConnexionOK())
			{
				//echo "connection ok";
				if($MeteoBDD->Lireall($tabData))
				{
					//echo "lecture reussi";

					if($MeteoBDD->Lireall($data))
					{
						foreach($tabData as $data)
						{
							echo "<p> Temperature :" . $data->temperature. "</p>"; 

							echo "<p> Pression :" .  $data->pression. "</p>"; 

							echo "<p> Humidité :" . $data->humidite. "</p>"; 

							echo "<p> Point de Rosee :" . $data->rosee. "</p>"; 

							echo "<p> Direction du vent :". $data->dir_vent. "</p>"; 
	
							echo "<p> Vitesse du vent : ".  $data->vit_vent . "</p>"; 
							echo "<br>";

						}
						
					}

					else
					{
						echo $MeteoBDD->getMessageErreur();
					}
				}

				else
				{
					echo $MeteoBDD->getMessageErreur();
				}
			}

			else
			{
				echo $MeteoBDD->getMessageErreur();
			}
		
			 
		 ?>



		</article>
	</div>
	
	<footer>
		Copyright section BTS SNIR lycée Georges Brassens
	</footer>

</body>

</html>